﻿using System;

public class Game
{
    public delegate void newRound(object source, EventArgs args);
    public event newRound newRoundEvent;

    public void OnNewRoundEvent()
    {
        if (newRoundEvent != null)
            newRoundEvent(this, EventArgs.Empty);
    }
}

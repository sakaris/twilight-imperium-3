﻿class HomeSystem : Systems
{
    public Race race;
    public HomeSystem(string _raceName = "")
    {
        race = new Race(_raceName);
    }

    public HomeSystem(Race _race)
    {
        race = _race;
    }
}
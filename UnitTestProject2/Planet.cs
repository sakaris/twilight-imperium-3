﻿using System;
using System.Collections.Generic;

//Should Inherit Interface remove Units, like Systems because troops and Ships behave the same

public class Planet
{
    public string name;
    public int resourceValue;
    public int influenceValue;
    private TechnologySpeciality myTech;
    public List<Troops> troops;
    public struct TechnologySpeciality
    {
        public int red;
        public int blue;
        public int green;
        public int yellow;

        public TechnologySpeciality(int _red = 0, int _blue = 0, int _green = 0, int _yellow = 0)
        {
            red = _red;
            blue = _blue;
            green = _green;
            yellow = _yellow;
        }
    }
    //private List<Troops> troops;


    public Planet(string _name = "NN", int _resourceValue = 0, int _influenceValue = 0,
                  TechnologySpeciality _technology = new TechnologySpeciality())
    {
        name = _name;
        resourceValue = _resourceValue;
        influenceValue = _influenceValue;
        myTech = _technology;
    }

    public TechnologySpeciality getTechnology() //could use a get function like in Player.cs
    {
        return myTech;
    }
    public void setTroops(string s = "")
    {
        if (troops == null)
            troops = new List<Troops>();

        if (s == "PDS")
            checkPDSLimitation();

        if (s == "Space Dock")
            checkSpaceDockLimitation();

        troops.Add(new Troops(s));
    }
    public void setTroops(Troops t, string s = "")
    {
        if (troops == null)
            troops = new List<Troops>();

        if (t.name == "PDS")
            checkPDSLimitation();

        if (t.name == "Space Dock")
            checkSpaceDockLimitation();

        troops.Add(t);
    }

    private void checkPDSLimitation()
    {
        int counter = 1;
        for (int i = 0; i < troops.Count; i++)
        {
            if (troops[i].name == "PDS" && counter < 2)
                counter++;
            else
                throw new Exception(message: "Theres already 2 PDS on this Planet");
        }
    }

    private void checkSpaceDockLimitation()
    {
        int counter = 0;
        for (int i = 0; i < troops.Count; i++)
        {
            if (troops[i].name == "SpaceDock" && counter == 0)
                counter++;
            else
                throw new Exception(message: "Theres already a Space Dock on this Planet");
        }
    }

    public void removeTroops(string s = "")
    {
        if (troops == null)
            troops = new List<Troops>();

        int index = findTroopByName(s);
        troops.RemoveAt(index);
    }

    public int findSpaceDock()
    {
        for (int i = 0; i < troops.Count; i++)
        {
            if (troops[i].name == "Space Dock")
                return i;
        }
        return -1;
    }

    private int findTroopByName(string s)
    {
        for (int i = 0; i < troops.Count; i++)
        {
            if (troops[i].name == s)
                return i;
        }
        return -1;
    }
}
﻿using System;
using System.Collections.Generic;


class Player
{
    public string name { get; set; }
    public Race race;
    public string color;
    public List<Systems> unusedSystems = new List<Systems>();
    public List<Planet> unExhaustedPlanets = new List<Planet>();
    public List<Planet> exhaustedPlanets = new List<Planet>();
    public List<Planet> allPlanets = new List<Planet>();
    private Planet.TechnologySpeciality techAdvantageStruct;
    public Planet.TechnologySpeciality technologyAdvantages
    {
        get
        {
            techAdvantageStruct = new Planet.TechnologySpeciality();
            calculateTechnologyAdvantage();
            return techAdvantageStruct;
        }
        set { techAdvantageStruct = value; }
    }
    private void calculateTechnologyAdvantage()
    {
        DelegateLoopPlanets inputDel = (i, k) =>
        {
            techAdvantageStruct.blue += unusedSystems[i].planets[k].getTechnology().blue;
            techAdvantageStruct.red += unusedSystems[i].planets[k].getTechnology().red;
            techAdvantageStruct.green += unusedSystems[i].planets[k].getTechnology().green;
            techAdvantageStruct.yellow += unusedSystems[i].planets[k].getTechnology().yellow;
        };

        loopAllPlanets(inputDel);
    }


    public Player(string _name = "")
    {
        name = _name;

    }
    public Player(string _color = "black", string _name = "")
    : this(_name)
    {
        color = _color;
    }
    public Player(Race _race, string _color = "black", string _name = "")
        : this(_name)
    {
        race = _race;
        color = _color;
    }


    public void initializeNewRound() //For test purposes only
    {
        DelegateLoopPlanets inputDel = (i, k) =>
        {
            unExhaustedPlanets.Add(unusedSystems[i].planets[k]);
            allPlanets.Add(unusedSystems[i].planets[k]);
        };
        loopAllPlanets(inputDel);
    }
    public void exhaustPlanet(string _planetName)
    {
        int index = findExhaustedPlanetByName(_planetName);
        exhaustedPlanets.Add(unExhaustedPlanets[index]);
        unExhaustedPlanets.RemoveAt(index);
    }
    private int findExhaustedPlanetByName(string _planetName)
    {
        for (int i = 0; i < unExhaustedPlanets.Count; i++)
        {
            if (unExhaustedPlanets[i].name == _planetName)
                return i;
        }
        return -1; // This results in an error
    }// same as findplanetbyname
    private int findPlanetByName(string _planetName)// same as exhausted
    {
        for (int i = 0; i < allPlanets.Count; i++)
        {
            if (allPlanets[i].name == _planetName)
                return i;
        }
        return -1; // This results in an error
    }
    public void produceUnits(Planet planet, Troops sourceSpaceDock, Troops unitToProduce)
    {
        int planetIndex = findPlanetByName(planet.name);
        int spaceDockIndex = allPlanets[planetIndex].findSpaceDock();

        if (allPlanets[planetIndex].troops[spaceDockIndex].abilities[0].capacity > 0)
        {
            allPlanets[planetIndex].troops.Add(unitToProduce);
            allPlanets[planetIndex].troops[spaceDockIndex].abilities[0].capacity--;
        }
        else
            throw new Exception("Capacity limit reached");
    }

    public delegate void DelegateLoopPlanets(int i, int k);
    private void loopAllPlanets(DelegateLoopPlanets inputMethodDelegate)
    {
        for (int i = 0; i < unusedSystems.Count; i++)
        {
            for (int k = 0; k < unusedSystems[i].planets.Count; k++)
            {
                inputMethodDelegate(i, k);
            }
        }
    }
    public delegate void canPassAsteroidsHandler(object source, EventArgs args);
    public event canPassAsteroidsHandler CanPassAsteroids;
    public void OnCanPassAsteroids()
    {
        if (CanPassAsteroids != null)
            CanPassAsteroids(this, EventArgs.Empty);
    }


    public void OnNextRound(object o, EventArgs args)
    {
        for (int i = 0; i < exhaustedPlanets.Count; i++)
            unExhaustedPlanets.Add(exhaustedPlanets[i]);
    }
}
﻿class Race
{
    public string name;//public for test purposes, would need a get function otherwise
    public HomeSystem homeSystem;//public for test purposes, would need a get function otherwise
    //public List<Abilities> abilities;
    //public List<Units> startUnits;

    public Race(string _name = "NN")
    {
        name = _name;
    }
    public Race(HomeSystem _homeSystem, string _name = "NN") : this(_name)
    {
        homeSystem = _homeSystem;
    }
}
﻿using System.Collections.Generic;

class RegularSystem : Systems
{
    public bool hasWormhole;
    public string wormhole;

    public RegularSystem(string _wormhole = null)
    {
        if (_wormhole != null)
            setWormhole(_wormhole);
    }
    public RegularSystem(Planet _planet, string _wormhole = null) 
        : this(_wormhole)
    {
        planets = new List<Planet>();
        planets.Add(_planet);
    }
    public RegularSystem(Planet _planet, Planet _planet2, string _wormhole = null) 
        : this(_planet, _wormhole)
    {
        planets.Add(_planet2);
    }
    public RegularSystem(Planet _planet, Planet _planet2, Planet _planet3, string _wormhole = null)
        : this(_planet, _planet2, _wormhole)
    {
        planets.Add(_planet3);
    }
    private void setWormhole(string _wormhole)
    {
        wormhole = _wormhole;
        hasWormhole = true;
    }

    public bool isEmptySystem()
    {
        if (hasNoPlanets())
            return true;
        return false;
    }

    private bool hasNoPlanets()
    {
        if (planets == null)
            return true;
        return planets.Count == 0;
    }

}
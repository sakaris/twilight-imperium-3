﻿using System;
using System.Collections.Generic;

public abstract class SAbilites 
{
    public abstract void effect();

    public class Capacity : SAbilites
    {
        public override void effect()
        {
            throw new NotImplementedException();
        }
    }
    public class SustainDamage : SAbilites
    {
        public override void effect()
        {
            throw new NotImplementedException();
        }
    }
    public class Bombardment : SAbilites
    {
        public override void effect()
        {
            throw new NotImplementedException();
        }
    }

    public class AntiFighterBarrage : SAbilites
    {
        public override void effect()
        {
            throw new NotImplementedException();
        }
    }
}
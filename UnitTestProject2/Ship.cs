﻿using System.Collections.Generic;

public class Ship : Unit
{
    public int movement;
    List<SAbilites> abilites;

    public Ship(string _name = "")
    {
        name = _name;
    }

    public Ship(string _name, int _movement = 1)
        : this(_name)
    {
        movement = _movement;
    }
    public Ship(string _name, SAbilites _ability, int _movement = 1)
        : this(_name, _movement)
    {
        if (abilites == null)
            abilites = new List<SAbilites>();
        abilites.Add(_ability);
    }
    public Ship(string _name, SAbilites _ability, SAbilites _ability2, int _movement = 1)
        : this(_name, _ability, _movement)
    {
        abilites.Add(_ability2);
    }
    public Ship(string _name, SAbilites _ability, SAbilites _ability2, SAbilites _ability3, int _movement = 1)
        : this(_name, _ability, _ability2 ,_movement)
    {
        abilites.Add(_ability3);
    }
}
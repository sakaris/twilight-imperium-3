﻿using System;
using System.Collections.Generic;

abstract class SpecialSystem : Systems
{
    public string name;//public for test purposes, would need a get function otherwise

    public SpecialSystem()
    {
    }

    public abstract void effect();

    public class AsteroidField : SpecialSystem
    {
        public List<Player> whoCanPass = new List<Player>();// not implemented

        public AsteroidField()
        {
            name = "Asteroid Field";
        }

        public override void effect()
        {
            throw new NotImplementedException();
        }

        public void OnPlayerCanPassAsteroids(object source, EventArgs args)
        {
            whoCanPass.Add((Player)source);
        }
    }
    public class Supernova : SpecialSystem
    {
        public Supernova()
        {
            name = "Supernova";
        }
        public override void effect()
        {
            throw new NotImplementedException();
        }
    }
    public class GravityRift : SpecialSystem
    {
        public GravityRift()
        {
            name = "GravityRift";
        }
        public override void effect()
        {
            throw new NotImplementedException();
        }
    }
    public class Nebula : SpecialSystem
    {
        public Nebula()
        {
            name = "Nebula";
        }
        public override void effect()
        {
            throw new NotImplementedException();
        }
    }
}
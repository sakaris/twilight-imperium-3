﻿using System;
using System.Collections.Generic;


class Starmap
{
    public List<Systems> unusedSystems = new List<Systems>();//public for test purposes, would need a get function otherwise
    public List<Systems> usedSystems = new List<Systems>();//public for test purposes, would need a get function otherwise
    private static Random rnd = new Random();

    public Starmap()
    {
        generateUnusedSystems();
    }

    private void generateUnusedSystems()
    {
        for (int i = 0; i < 10; i++)
        {
            unusedSystems.Add(new RegularSystem(new Planet()));
            unusedSystems.Add(new RegularSystem(new Planet(), new Planet()));
        }
        for (int i = 0; i < 6; i++)
        {
            unusedSystems.Add(new RegularSystem());
        }
        for (int i = 0; i < 2; i++)
        {
            unusedSystems.Add(new SpecialSystem.AsteroidField());
            unusedSystems.Add(new SpecialSystem.Nebula());
            unusedSystems.Add(new SpecialSystem.Supernova());
        }
    }

    public void calculateSystems(List<Player> _players)
    {
        switch (_players.Count)
        {
            case 3:
                RemoveSystemFromUnusedSystems(new SpecialSystem.AsteroidField());
                RemoveSystemFromUnusedSystems(3, new RegularSystem());
                randomRemoveSystemFromUnusedSystems(4);
                break;
            case 4:
                break;
            case 5:
                randomRemoveSystemFromUnusedSystems(1);
                break;
            case 6:
                randomRemoveSystemFromUnusedSystems(2);
                break;
            default:
                break;
        }
    }

    private void RemoveSystemFromUnusedSystems(int amount, Systems system)
    {
        for (int i = 0; i < 3; i++)
            unusedSystems.RemoveAt(0);
    }

    private void RemoveSystemFromUnusedSystems(Systems system)
    {
        int index = findSystemInUnusedSystems(system);
        unusedSystems.RemoveAt(index);
    }

    private int findSystemInUnusedSystems(Systems system)
    {
        for (int i = 0; i + 1 < unusedSystems.Count; i++)
        {
            if (unusedSystems[i].GetType() == system.GetType())
                return i;
        }
        return 0;
    }

    private void randomRemoveSystemFromUnusedSystems(int amount)
    {
        for (int i = 0; i < amount; i++)
            unusedSystems.RemoveAt(rnd.Next(0, unusedSystems.Count));
    }

    public void distributeSystems(List<Player> _players)
    {
        int systemsPerPlayer = unusedSystems.Count / _players.Count;

        for (int i = 0; i < _players.Count; i++)
        {
            for (int k = 0; k < systemsPerPlayer; k++)
            {
                _players[i].unusedSystems.Add(unusedSystems.PopRandom());
            }
        }
    }

    public List<Systems> freeSystems()
    {
        throw new NotImplementedException();
    }

    public void placeSystems(Systems desSystem, int direction, Systems newSystem)
    {
        throw new NotImplementedException();
    }
}

public static class MyExtensions
{
    static Random rnd = new Random();

    public static Systems PopRandom<Systems>(this List<Systems> starmap)
    {
        int i = rnd.Next(0, starmap.Count);
        object o = starmap[i];
        starmap.RemoveAt(i);
        return (Systems)o;
    }
}
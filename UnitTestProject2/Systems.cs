﻿using System;
using System.Collections.Generic;

//Should Inherit Interface remove Units, like Planet because troops and Ships behave the same
class Systems
{
    public List<Planet> planets; //public for test purposes, would need a get function otherwise
    public List<Ship> ships;

    public Systems()
    {
    }

    public Systems(Planet _planet)
    {
        planets = new List<Planet>();
        planets.Add(_planet);
    }

    public void setShip(string s = "")
    {
        if (ships == null)
            ships = new List<Ship>();
        ships.Add(new Ship(s));
    }
    public void setShip(Ship s)
    {
        if (ships == null)
            ships = new List<Ship>();
        ships.Add(s);
    }

    public void removeShip(string s = "")
    {
        if (ships == null)
            ships = new List<Ship>();

        int index = findShipByName(s);
        ships.RemoveAt(index);
    }

    private int findShipByName(string s)
    {
        for (int i = 0; i < ships.Count; i++)
        {
            if (ships[i].name == s)
                return i;
        }
        return -1;
    }
}
﻿using System;

public abstract class TAbilities
{
    public int capacity;
    public abstract void effect();
    public abstract void OnNextRound(object o, EventArgs args);

    public class PlanetaryShield : TAbilities
    {
        public override void effect()
        {
            throw new NotImplementedException();
        }

        public override void OnNextRound(object o, EventArgs args)
        {
            ;
        }
    }
    public class Capacity : TAbilities
    {
        private int resourceValuePlanet;
        public Capacity(int _resourceValuePlanet)
        {
            resourceValuePlanet = _resourceValuePlanet;
            capacity = 6 + resourceValuePlanet;
        }
        public override void effect()
        {
        }

        public override void OnNextRound(object o, EventArgs args)
        {
            capacity = 6 + resourceValuePlanet;
        }
    }
}
﻿using System.Collections.Generic;

public class Troops : Unit
{
    public List<TAbilities> abilities;

    public Troops(string _name = "")
    {
        name = _name;
    }

    public Troops(TAbilities.Capacity _capacity, string _name = "") : this(_name)
    {
        if (abilities == null)
            abilities = new List<TAbilities>();
        abilities.Add(_capacity);
    }
    public Troops(TAbilities.PlanetaryShield _planetary, string _name = "") : this(_name)
    {
        if (abilities == null)
            abilities = new List<TAbilities>();
        abilities.Add(_planetary);
    }
    public Troops(TAbilities.Capacity _capacity, TAbilities.PlanetaryShield _planetary, string _name = "") : this(_name)
    {
        if (abilities == null)
            abilities = new List<TAbilities>();
        abilities.Add(_capacity);
        abilities.Add(_planetary);
    }
}
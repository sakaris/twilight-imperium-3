﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;


[TestClass]
public class UnitTest1
{
    Starmap genericStarmap = new Starmap();
    Systems genericSystems = new Systems();
    Planet genericPlanet = new Planet("NN");
    Planet planetWithAllTechnologies = new Planet(_technology: new Planet.TechnologySpeciality(_red: 1, _blue: 2, _green: 3, _yellow: 4));
    Planet planetWithGreenTechnology = new Planet(_technology: new Planet.TechnologySpeciality(_green: 2));
    SpecialSystem.AsteroidField SpecialSystemAsteroid = new SpecialSystem.AsteroidField();
    SpecialSystem.GravityRift SpecialSystemGravity = new SpecialSystem.GravityRift();
    SpecialSystem.Supernova SpecialSystemSupernova = new SpecialSystem.Supernova();
    SpecialSystem.Nebula SpecialSystemNebula = new SpecialSystem.Nebula();
    RegularSystem SystemRegular = new RegularSystem();
    HomeSystem SystemHome = new HomeSystem();

    internal List<Player> players;
    [TestInitialize]
    public void TestInitialize()
    {
        players = new List<Player>()
        {
                new Player("1"),
                new Player("2"),
                new Player("3"),
                new Player("4"),
                new Player("5"),
                new Player("6"),
        };

        players[0].unusedSystems.Add(new Systems(new Planet("Test1")));
        players[0].unusedSystems.Add(new Systems(new Planet("Test2")));
        players[0].unusedSystems.Add(new Systems(new Planet("Test3")));
        players[0].unusedSystems.Add(new Systems(new Planet("Test4")));

        players[0].initializeNewRound();
    }
    [TestMethod]
    public void newStarmapUsedSystemsIs0()
    {
        Assert.AreEqual(0, genericStarmap.usedSystems.Count);
    }

    [TestMethod]
    public void newStarmapUnusedSystemsIs32()
    {
        Assert.AreEqual(32, genericStarmap.unusedSystems.Count);
    }

    [TestMethod]
    public void newStarmapHasPlanetsLikeRules()
    {
        bool a = 2 == countEveryType(new SpecialSystem.AsteroidField());
        bool b = 2 == countEveryType(new SpecialSystem.Nebula());
        bool c = 2 == countEveryType(new SpecialSystem.Supernova());

        bool d = 6 == countEveryType(new RegularSystem(), true);
        bool e = 20 == countEveryType(new RegularSystem(new Planet()));
        //bool d = countEmptyFields() == 6;
        //bool e = countNonEmptyFields() == 20;

        Assert.AreEqual(true, a && b && c && d && e);
    }

    [TestMethod]
    public void calculateSystemsMethod()
    {
        genericStarmap.calculateSystems(players);
    }

    [TestMethod]
    public void CalculateSystemsSixPlayersUnusedShouldBe30()
    {
        genericStarmap.calculateSystems(players);
        Assert.AreEqual(30, genericStarmap.unusedSystems.Count);
    }

    [TestMethod]
    public void EmptySystemIsEmpty()
    {
        RegularSystem r1 = new RegularSystem();
        Assert.AreEqual(true, r1.isEmptySystem());
    }

    [TestMethod]
    public void SystemWithPlanetIsNotEmpty()
    {
        RegularSystem r1 = new RegularSystem(new Planet());
        Assert.AreEqual(false, r1.isEmptySystem());
    }

    [TestMethod]
    public void RegularSystemIsRegular()
    {
        RegularSystem r1 = new RegularSystem();
        Assert.AreEqual(new RegularSystem().GetType(), r1.GetType());
    }

    [TestMethod]
    public void HomeSystemIsHomeSystem()
    {
        HomeSystem h1 = new HomeSystem();
        Assert.AreEqual(new HomeSystem().GetType(), h1.GetType());
    }

    [TestMethod]
    public void AsteroidSpecialSystem()
    {
        Systems s1 = new SpecialSystem.AsteroidField();
    }

    [TestMethod]
    public void AsteroidSystemNameIsAsteroidSystem()
    {
        Systems a = new SpecialSystem.GravityRift();

        SpecialSystem.AsteroidField ssa = new SpecialSystem.AsteroidField();
        Assert.AreEqual(true, ssa.name == "Asteroid Field");
    }

    [TestMethod]
    public void ThreePlayerGameRemoves8Systems()
    {
        players.RemoveRange(0, 3);
        genericStarmap.calculateSystems(players);
        Assert.AreEqual(24, genericStarmap.unusedSystems.Count);
    }

    [TestMethod]
    public void ThreePlayerGameRemoveAtLeast1Astroid()
    {
        players.RemoveRange(0, 3);

        int before = countEveryType(new SpecialSystem.AsteroidField());
        genericStarmap.calculateSystems(players);
        int after = countEveryType(new SpecialSystem.AsteroidField());

        Assert.IsTrue(before > after);
    }

    [TestMethod]
    public void FourPlayerGameRemoveNoSystems()
    {
        players.RemoveRange(0, 2);

        genericStarmap.calculateSystems(players);

        Assert.AreEqual(32, genericStarmap.unusedSystems.Count);
    }

    [TestMethod]
    public void FivePlayerGameRemoveOneRandomSystem()
    {
        players.RemoveAt(0);

        genericStarmap.calculateSystems(players);

        Assert.AreEqual(31, genericStarmap.unusedSystems.Count);
    }

    [TestMethod]
    public void distributeSystems()
    {
        genericStarmap.distributeSystems(players);
    }

    [TestMethod]
    public void distributeSystemsSixPlayers()
    {
        players[0].unusedSystems.RemoveRange(0, 4);

        genericStarmap.calculateSystems(players);
        genericStarmap.distributeSystems(players);

        for (int i = 0; i < 6; i++)
        {
            Assert.AreEqual(5, players[i].unusedSystems.Count);
        }
    }

    [TestMethod]
    public void distributeSystemsFivePlayers()
    {
        players.RemoveAt(0);

        genericStarmap.calculateSystems(players);
        genericStarmap.distributeSystems(players);

        for (int i = 0; i < 5; i++)
        {
            Assert.AreEqual(6, players[i].unusedSystems.Count);
        }
    }

    [TestMethod]
    public void distributeSystemsFourPlayers()
    {
        players.RemoveRange(0, 2);

        genericStarmap.calculateSystems(players);
        genericStarmap.distributeSystems(players);

        for (int i = 0; i < 4; i++)
        {
            Assert.AreEqual(8, players[i].unusedSystems.Count);
        }
    }

    [TestMethod]
    public void distributeSystemsThreePlayers()
    {
        players.RemoveRange(0, 3);

        genericStarmap.calculateSystems(players);
        genericStarmap.distributeSystems(players);

        for (int i = 0; i < 3; i++)
            Assert.AreEqual(8, players[i].unusedSystems.Count);
    }

    /// <summary>
    /// This might fail because of randomness, running it 30 times usually is enough
    /// Try running the test again and see if it still fails!
    /// </summary>
    [TestMethod]
    public void SomeGamesHaveNoAsteroids()
    {
        int i = 0;
        players.RemoveRange(0, 3);

        for (; i < 30; i++)
        {
            genericStarmap = new Starmap();

            genericStarmap.calculateSystems(players);

            if (countEveryType(new SpecialSystem.AsteroidField()) == 0)
                break;
        }

        Assert.AreEqual(0, countEveryType(new SpecialSystem.AsteroidField()));
    }

    [TestMethod]
    public void newSystemWithPlanet()
    {
        Systems s1 = new Systems(planetWithAllTechnologies);
    }

    [TestMethod]
    public void newSystemWithPlanetHasTechAttribute()
    {
        Planet VEFUT = new Planet(_name: "VEFUT", _resourceValue: 2, _technology: new Planet.TechnologySpeciality(_red: 1));
        Systems s1 = new Systems(VEFUT);
        Planet pRed = new Planet(_technology: new Planet.TechnologySpeciality(_red: 1));

        bool b = pRed.getTechnology().blue == s1.planets[0].getTechnology().blue;
        bool c = pRed.getTechnology().green == s1.planets[0].getTechnology().green;
        bool d = pRed.getTechnology().red == s1.planets[0].getTechnology().red;
        bool e = pRed.getTechnology().yellow == s1.planets[0].getTechnology().yellow;

        Assert.AreEqual(true, b && c && d && e);
    }

    [TestMethod]
    public void PlayersTechAdvantageIsEqualToSystemPlanetsAdvantage()
    {
        Systems s1 = new Systems(planetWithAllTechnologies);
        Planet.TechnologySpeciality t1 = new Planet.TechnologySpeciality(_red: 1, _blue: 2, _green: 3, _yellow: 4);

        players[0].unusedSystems.Add(new Systems(new Planet()));
        players[0].unusedSystems.Add(s1);

        Assert.AreEqual(t1, players[0].technologyAdvantages);
    }

    [TestMethod]
    public void PlayerTechEqualToSumOfPlanetsTech()
    {
        Systems s1 = new Systems(planetWithAllTechnologies);
        Systems s2 = new Systems(planetWithGreenTechnology);

        Planet.TechnologySpeciality t1 = new Planet.TechnologySpeciality(_red: 1, _blue: 2, _green: 5, _yellow: 4);

        players[0].unusedSystems.Add(s1);
        players[0].unusedSystems.Add(s2);

        Assert.AreEqual(t1, players[0].technologyAdvantages);
    }

    [TestMethod]
    public void CallingGetPlayerTechStructTwiceDoesntIncreaseIt()
    {
        Systems s1 = new Systems(planetWithAllTechnologies);
        Systems s2 = new Systems(planetWithGreenTechnology);
        Planet.TechnologySpeciality t1 = new Planet.TechnologySpeciality(_red: 1, _blue: 2, _green: 5, _yellow: 4);

        players[0].unusedSystems.Add(s1);
        players[0].unusedSystems.Add(s2);

        Planet.TechnologySpeciality tx = players[0].technologyAdvantages;
        tx = players[0].technologyAdvantages;

        Assert.AreEqual(t1, players[0].technologyAdvantages);
    }

    [TestMethod]
    public void InitializeNewRoundExhaustedPlanetsIsAllPlanets()
    {

        Assert.AreEqual(4, players[0].unExhaustedPlanets.Count);
    }

    [TestMethod]
    public void ExhaustSpecificPlanet()
    {
        players[0].exhaustPlanet("Test1");

        Assert.AreEqual(3, players[0].unExhaustedPlanets.Count);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void FailWhenExhaustNotAvailablePlanet()
    {
        players[0].initializeNewRound();
        players[0].exhaustPlanet("Random Planet Name");
    }
    [TestMethod]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void FailWhenExhaustPlanetTwice()
    {
        players[0].exhaustPlanet("Test1");
        players[0].exhaustPlanet("Test1");
    }

    [TestMethod]
    public void HomeSystemHasRace()
    {
        HomeSystem h1 = new HomeSystem("XXCHAR");
        Race r1 = h1.race;
        Assert.AreEqual(r1, h1.race);
    }

    [TestMethod]
    public void RaceHasHomeSystem()
    {
        Race r1 = new Race(new HomeSystem());
        HomeSystem h1 = r1.homeSystem;
        Assert.AreEqual(h1, r1.homeSystem);
    }

    [TestMethod]
    public void InitializeHomeSystemWithRace()
    {
        Race r1 = new Race(new HomeSystem());
        HomeSystem h1 = new HomeSystem(r1);
    }

    [TestMethod]
    public void RegularSystemWithWormhole()
    {
        RegularSystem r0 = new RegularSystem("a");
    }

    [TestMethod]
    public void RegularSystemWithWormholeHasFlag()
    {
        RegularSystem r0 = new RegularSystem("a");
        Assert.AreEqual(true, r0.hasWormhole);
    }

    [TestMethod]
    public void RegularSystemWithWormholeHasString()
    {
        RegularSystem r0 = new RegularSystem("a");
        Assert.AreEqual("a", r0.wormhole);
    }

    [TestMethod]
    public void AddExhaustedPlanetWhenExhaustPlanet()
    {
        players[0].exhaustPlanet("Test1");

        Assert.AreEqual(true, players[0].exhaustedPlanets.Count > 0);
    }

    [TestMethod]
    public void allPlanetsContainsAllPlanetsOfPlayer()
    {
        Assert.AreEqual(4, players[0].allPlanets.Count);
    }

    [TestMethod]
    public void allPlanetsStaysWhenPlanetIsExhausted()
    {
        players[0].exhaustPlanet("Test1");

        Assert.AreEqual(4, players[0].allPlanets.Count);
    }

    [TestMethod]
    public void InitializePlayerWithRace()
    {
        players[0] = new Player(new Race());
    }

    [TestMethod]
    public void InitializePlayerWithColor()
    {
        players[0] = new Player(_color: "Red");
    }

    [TestMethod]
    public void PlayerWithColorAndRaceHasColorAndRace()
    {
        players[0] = new Player(new Race(), _color: "Red");

        Assert.AreEqual("Red", players[0].color);
        Assert.AreEqual(true, players[0].color != null);
        Assert.AreEqual(true, players[0].race != null);
    }

    [TestMethod]
    public void EventCanPassAsteroidsSetsListToNotZero()
    {
        SpecialSystem.AsteroidField asteroids = new SpecialSystem.AsteroidField();
        players[0].CanPassAsteroids += asteroids.OnPlayerCanPassAsteroids;
        players[0].CanPassAsteroids += asteroids.OnPlayerCanPassAsteroids;
        players[0].OnCanPassAsteroids();

        Assert.AreEqual(true, 0 < asteroids.whoCanPass.Count);
    }

    [TestMethod]
    public void NewPlanetTroopsListIsNull()
    {
        Assert.AreEqual(true, null == genericPlanet.troops);
    }

    [TestMethod]
    public void AddTroopsToPlanet()
    {
        genericPlanet.setTroops();
        Assert.AreEqual(1, genericPlanet.troops.Count);
    }

    [TestMethod]
    public void SetSpecificTroop()
    {
        Troops g1 = new Troops("Test1");
        genericPlanet.setTroops(g1);
        Assert.AreEqual("Test1", genericPlanet.troops[0].name);
    }

    [TestMethod]
    public void RemoveTroopsFromPlanet()
    {
        genericPlanet.setTroops();
        genericPlanet.removeTroops();
        Assert.AreEqual(0, genericPlanet.troops.Count);
    }

    [TestMethod]
    public void RemoveSpecificTroop()
    {
        Troops g1 = new Troops("Ground Force");
        genericPlanet.setTroops(g1);
        genericPlanet.removeTroops("Ground Force");
        Assert.AreEqual(0, genericPlanet.troops.Count);
    }
    [TestMethod]
    public void AddShipToSystem()
    {
        Ship s1 = new Ship();
        Systems s = new Systems();
        s.setShip();

        Assert.AreEqual(1, s.ships.Count);
    }

    [TestMethod]
    public void SetSpecificShipToSystem()
    {
        Ship s1 = new Ship("Cruiser");
        genericSystems.setShip(s1);

        Assert.AreEqual(1, genericSystems.ships.Count);
        Assert.AreEqual("Cruiser", genericSystems.ships[0].name);
    }

    [TestMethod]
    public void RemoveShipFromSystem()
    {
        Ship s1 = new Ship();
        genericSystems.setShip(s1);
        genericSystems.setShip(s1);
        genericSystems.removeShip();

        Assert.AreEqual(1, genericSystems.ships.Count);
    }

    [TestMethod]
    public void RemoveSpecificShipFromSystem()
    {
        string s = "Cruiser";
        Ship s1 = new Ship(s);
        genericSystems.setShip(s1);
        genericSystems.setShip(new Ship("Not Cruiser"));
        genericSystems.removeShip(s);

        Assert.AreEqual(1, genericSystems.ships.Count);
        Assert.AreNotEqual(s, genericSystems.ships[0].name);
    }

    [TestMethod]
    public void ShipCanHaveDifferentProperties()
    {
        Ship sh0 = new Ship("name");
        Ship sh1 = new Ship("name 2", 2);
        Ship sh2 = new Ship("name", new SAbilites.AntiFighterBarrage());
        Ship sh3 = new Ship("name", new SAbilites.AntiFighterBarrage(), new SAbilites.Bombardment(), new SAbilites.SustainDamage());
    }

    [TestMethod]
    public void TroopsCanHaveAbilities()
    {
        Troops t1 = new Troops(_name: "PDS", _planetary: new TAbilities.PlanetaryShield());
    }

    [TestMethod]
    [ExpectedException(typeof(Exception))]
    public void PDSLimitations()
    {
        Planet p0 = new Planet();
        p0.setTroops("PDS");
        p0.setTroops("PDS");
        p0.setTroops("PDS");
    }
    [TestMethod]
    [ExpectedException(typeof(Exception))]
    public void PDSLimitationsSecondConstructor()
    {
        Planet p1 = new Planet();
        Troops pds = new Troops("PDS");
        p1.setTroops(pds);
        p1.setTroops(pds);
        p1.setTroops(pds);
    }

    [TestMethod]
    [ExpectedException(typeof(Exception))]
    public void OnlyOneSpaceDockPerPlanet()
    {
        Planet p0 = new Planet();
        p0.setTroops("Space Dock");
        p0.setTroops("Space Dock");
    }

    [TestMethod]
    [ExpectedException(typeof(Exception))]
    public void OnlyOneSpaceDockPerPlanetSecondConstructor()
    {
        Planet p1 = new Planet();
        Troops spaceDock = new Troops("Space Dock");
        p1.setTroops(spaceDock);
        p1.setTroops(spaceDock);
    }

    [TestMethod]
    public void SettingSpaceDockOnPlanetOfPlayer()
    {
        Planet p1 = new Planet();
        Troops spaceDock = new Troops("Space Dock");
        players[0] = new Player("New Player");
        p1.setTroops(spaceDock);
        players[0].allPlanets.Add(p1);

        Assert.AreEqual("Space Dock", players[0].allPlanets[0].troops[0].name);
    }

    [TestMethod]
    public void PlayerProduceUnitInSpaceDock()
    {
        Planet p1 = new Planet();
        Troops spaceDock = new Troops(new TAbilities.Capacity(p1.resourceValue),"Space Dock");
        players[0] = new Player("New Player");
        p1.setTroops(spaceDock);
        players[0].allPlanets.Add(p1);

        players[0].produceUnits(p1, p1.troops[0], new Troops("Ground Force"));

        Assert.AreEqual("Ground Force", players[0].allPlanets[0].troops[1].name);
    }

    [TestMethod]
    [ExpectedException(typeof(Exception))]
    public void SpaceDockCapacityLimitReached()
    {
        Planet p1 = new Planet();
        Troops spaceDock = new Troops(new TAbilities.Capacity(p1.resourceValue),"Space Dock");
        players[0] = new Player("New Player");
        p1.setTroops(spaceDock);
        players[0].allPlanets.Add(p1);

        for (int i = 0; i < 7; i++)
            players[0].produceUnits(p1, p1.troops[0], new Troops("Ground Force"));
    }

    [TestMethod]
    public void SpaceDockPlusResourceValueOfPlanetIsCapacity()
    {
        Planet p0 = new Planet(_resourceValue: 3);
        Troops spaceDock = new Troops(new TAbilities.Capacity(p0.resourceValue), "Space Dock");

        p0.setTroops(spaceDock);

        Assert.AreEqual(9, p0.troops[0].abilities[0].capacity);
    }

    [TestMethod]
    public void SpaceDockCanProduceMoreOnHighResourcePlanet()
    {
        Planet p1 = new Planet(_resourceValue: 5);
        Troops spaceDock = new Troops(new TAbilities.Capacity(p1.resourceValue), "Space Dock");
        players[0] = new Player("New Player");

        p1.setTroops(spaceDock);
        players[0].allPlanets.Add(p1);

        for (int i = 0; i < 10; i++)
            players[0].produceUnits(p1, p1.troops[0], new Troops("Ground Force"));
    }

    [TestMethod]
    public void EventNewGame()
    {
        Game g = new Game();
        TAbilities.PlanetaryShield a = new TAbilities.PlanetaryShield();
        g.newRoundEvent += a.OnNextRound;
        g.OnNewRoundEvent();
    }

    [TestMethod]
    public void EventNewGameResetsCapacity()
    {
        Planet p1 = new Planet(_resourceValue: 5);
        Troops spaceDock = new Troops(new TAbilities.Capacity(p1.resourceValue), "Space Dock");
        players[0] = new Player("New Player");

        p1.setTroops(spaceDock);
        players[0].allPlanets.Add(p1);

        for (int i = 0; i < 10; i++)
            players[0].produceUnits(p1, p1.troops[0], new Troops("Ground Force"));

        Game g = new Game();

        g.newRoundEvent += p1.troops[0].abilities[0].OnNextRound;
        g.OnNewRoundEvent();

        for (int i = 0; i < 10; i++)
            players[0].produceUnits(p1, p1.troops[0], new Troops("Ground Force"));
    }

    [TestMethod]
    public void EventNewGamePlayerExhaustedPlanetsGoUnexhausted()
    {
        Game g = new Game();
        genericStarmap.calculateSystems(players);
        genericStarmap.distributeSystems(players);
        players[0].unExhaustedPlanets.Add(genericPlanet);
        players[0].exhaustPlanet("NN");

        g.newRoundEvent += players[0].OnNextRound;
        g.OnNewRoundEvent();

        players[0].exhaustPlanet("NN");
    }


    private delegate int DelegateCountSystems(int position, int counter);
    private int countEveryType(Systems inputSystem, bool shallBeEmpty = false) //For test purposes only
    {
        object o = getMethodType(inputSystem);

        DelegateCountSystems inputDel = delegateFunctionCountEveryType(shallBeEmpty, o);

        return countInAllUnusedSystemsOfSmap(inputDel);
    }
    private int countInAllUnusedSystemsOfSmap(DelegateCountSystems inputMethodDelegate)
    {
        int counter = 0;

        for (int i = 0; i < genericStarmap.unusedSystems.Count; i++)
            counter += inputMethodDelegate(i, counter);

        return counter;
    }
    private DelegateCountSystems delegateFunctionCountEveryType(bool shallBeEmpty, object o)//try exclude lambda
    {
        return (i, counter) =>
        {
            if (genericStarmap.unusedSystems[i].GetType() == o.GetType())
            {
                if (shallBeEmpty)
                {
                    if (genericStarmap.unusedSystems[i].planets == null)
                        return 1;
                }
                else
                {
                    if (o.GetType() == SystemRegular.GetType() && genericStarmap.unusedSystems[i].planets == null)
                        return 0;
                    return 1;
                }
            }

            return 0;
        };
    }
    private object getMethodType(Systems inputSystem)
    {
        object o;
        if (inputSystem.GetType() == SpecialSystemAsteroid.GetType())
            o = SpecialSystemAsteroid;
        else if (inputSystem.GetType() == SpecialSystemGravity.GetType())
            o = SpecialSystemGravity;
        else if (inputSystem.GetType() == SpecialSystemNebula.GetType())
            o = SpecialSystemNebula;
        else if (inputSystem.GetType() == SpecialSystemSupernova.GetType())
            o = SpecialSystemSupernova;
        else if (inputSystem.GetType() == SystemRegular.GetType())
            o = SystemRegular;
        else if (inputSystem.GetType() == SystemRegular.GetType())
            o = SystemRegular;
        else if (inputSystem.GetType() == SystemHome.GetType())
            o = SystemHome;
        else
            o = new Systems();
        return o;
    }
}
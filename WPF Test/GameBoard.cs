using System;
using System.Windows;
using System.Collections;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

class Program
{
    public static void main()
    {
        var board = new GameBoard(6);
        foreach (Tile neighbour in board.tiles[0].neighbouringTiles)
        { Console.WriteLine(neighbour.nr); }
        foreach (Tile neighbour in board.tiles[36].neighbouringTiles)
        { Console.WriteLine(neighbour.nr); }
        //Console.ReadKey();
    }
}

class GameBoard : Object
{
    // Instanzvariablen
    public Tile[] tiles = new Tile[37];



    public GameBoard(int numberOfPlayers)
    {
        // Mercantor Rex generieren
        tiles[0] = new Tile(new Vector(4, 3.5));
        for (int a = 1; a < 37; a++)
        { tiles[a] = null; }

        // Restliche Tiles generieren
        // F�r die ersten 19 Tiles Nachbarn generieren
        for (int a = 0; a < 19; a++)
        {
            // F�r jede Tile alle benachbarten Tiles erstellen
            foreach (Vector vector in tiles[a].neighbouringVectors)
            {
                Tile tile = new Tile(vector);
                if (Array.Exists(tiles, element => element == tile))
                {
                    // Tilecounter reduzieren, falls Tile bereits existiert
                    Tile.DecrementTileCounter();
                    Console.WriteLine("Tile nicht hinzugef�gt: " + Convert.ToString(tile.nr));
                }
                else
                {
                    // Tile zu Tiles hinzuf�gen, falls noch keine Tile mit diesem Vektor existiert
                    tiles[Array.IndexOf(tiles, null)] = tile;
                    Console.WriteLine("Tile hinzugef�gt: " + Convert.ToString(tile.nr));
                }
            }
        }

        // Je nach Spielerzahl ungenutzte Kacheln entfernen

        // Jeder Kachel ihre Nachbarn zuweisen
        foreach (Tile tile in tiles)
        {
            foreach (Vector vec in tile.neighbouringVectors)
            {
                for (int i = 0; i < 37; i++)
                {
                    if (vec == tiles[i].vector)
                    {
                        tile.neighbouringTiles.Add(tiles[i]);
                    }
                }
            }
        }
    }

}

class Tile : Object
{
    // Statische Klassenvariablen
    public static int tileCounter = 0;
    public static Vector[] directions = { new Vector(-1, 0.5), new Vector(0, 1), new Vector(1, 0.5), new Vector(1, -0.5), new Vector(0, -1), new Vector(-1, -0.5) };

    private Canvas mainCanvas;
    private Polygon polygon;

    //Instanzvariablen
    public Vector vector;
    public int nr;
    public ArrayList neighbouringTiles = new ArrayList();
    public Vector[] neighbouringVectors = new Vector[6];

    public void drawSelfOnto(Canvas mainCanvas)
    {
        this.mainCanvas = mainCanvas;

        PointCollection pointCollection = new PointCollection();

        double scaleFactor = 8;
        pointCollection.Add(new Point(scaleFactor * 4, scaleFactor * 7));
        pointCollection.Add(new Point(scaleFactor * 8, scaleFactor * 0));
        pointCollection.Add(new Point(scaleFactor * 4, scaleFactor * -7));
        pointCollection.Add(new Point(scaleFactor * -4, scaleFactor * -7));
        pointCollection.Add(new Point(scaleFactor * -8, scaleFactor * 0));
        pointCollection.Add(new Point(scaleFactor * -4, scaleFactor * 7));

        //polygon.Fill = Brushes.DarkBlue;
        polygon.Fill = new ImageBrush(new BitmapImage(new Uri($"pack://application:,,,/img/pic1399245.jpg")));

        polygon.Points = pointCollection;

        Vector newVector = Vector.Add(new Vector(4, 4), vector);
        Canvas.SetTop(polygon, newVector.Y * 120);
        Canvas.SetLeft(polygon, newVector.X * 100 + 300); // offset: 300

        adjustCanvasHeightWidthIfPolygonIsOutOfBounce(mainCanvas, newVector);

        mainCanvas.Children.Add(polygon);
    }
    #region Helper draw
    private static void adjustCanvasHeightWidthIfPolygonIsOutOfBounce(Canvas mainCanvas, Vector newVector)
    {
        if (newVector.X * 100 + 300 > mainCanvas.ActualWidth)
            mainCanvas.Width = newVector.X * 100 + 450;
        if (newVector.Y * 120 > mainCanvas.ActualHeight)
            mainCanvas.Height = newVector.Y * 120 + 150;
    }
    #endregion

    public Tile(Vector vec)
    {

        polygon = new Polygon();

        vector = vec;
        nr = ++tileCounter;

        // neighBouringVectors generieren
        for (int a = 0; a < 6; a++)
        {
            neighbouringVectors[a] = vector + directions[a];
        }
    }

    public Tile(Vector vec, Canvas mainCanvas)
    {
        polygon = new Polygon();

        vector = vec;
        nr = ++tileCounter;

        drawSelfOnto(mainCanvas);
        // neighBouringVectors generieren
        for (int a = 0; a < 6; a++)
        {
            neighbouringVectors[a] = vector + directions[a];
        }
    }

    //~Tile()
    //{ Tile.tileCounter--; }

    public static void DecrementTileCounter()
    {
        tileCounter--;
    }

    public static bool operator ==(Tile tileA, Tile tileB)
    {
        try
        {
            return (tileA.vector == tileB.vector);
        }
        catch
        {
            return false;
        }
    }

    public static bool operator !=(Tile tileA, Tile tileB)
    {
        try { return !(tileA.vector == tileB.vector); }
        catch { return false; }
    }
}
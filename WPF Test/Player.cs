﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

class Player
{
    private String name;
    private Systems homesystem;

    public Player(String pName, Systems pHomesystem)
    {
        this.name = pName;
        this.homesystem = new Systems(name, new Vector(4, 4));
    }

    public Systems getHomesystem()
    {
        return homesystem;
    }
}


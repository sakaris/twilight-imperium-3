﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows;

class Starmap
{
    private Systems[] systemsList;
    private Canvas starmapShape;

    public Starmap()
    {
        this.systemsList = new Systems[37];
        this.starmapShape = new Canvas();
        this.initSystemsOld();
        this.paintStarmap();
    }

    private void initSystems()
    {
        systemsList[0] = new Systems("1", new Vector(0, 0));
        int counter = 1;
        // jedes System sollte mindestens einmal durchlaufen werden
        for (int sysNr = 0; sysNr < systemsList.Length; sysNr++)
        {
            // in leeren Systemen kann nichts durchlaufen werden
            if (systemsList[sysNr] != null)
            {
                // in jedem System, sollte mindestens einmal jede Richtung abgelaufen werden
                for (int dirNr = 0; dirNr < Systems.directions.Length; dirNr++)
                {
                    // ist die Richtung des Systems noch mit keinem Nachbarn besetzt
                    if (systemsList[sysNr].getNeighbours()[dirNr] == null)
                    {
                        // neuer potenzeller Vector für einen direkten Nachbarn
                        Vector calcVector = Vector.Add(systemsList[sysNr].getVector(), Systems.directions[dirNr]);
                        if (checkVector(calcVector))
                        {
                            systemsList[counter] = new Systems(counter.ToString(), calcVector);
                            Systems neighSystem = getSystemByVector(calcVector);
                            if (neighSystem != null)
                            {
                                // hier fehlt noch ein wenig Magie
                                systemsList[sysNr].getNeighbours()[dirNr] = neighSystem;
                                neighSystem.getNeighbours()[(dirNr + 3) % 6] = systemsList[sysNr];
                            }
                            counter++;
                        }
                    }
                }
            }
        }
    }

    /// <summary>
	/// Check, if the given vector is legit on starmap (3 full rings).
	/// </summar>
    private Boolean checkVector(Vector pVector)
    {
        if (Math.Abs(pVector.X) > 3 || Math.Abs(pVector.Y) > 3)
            return false;
        if (Math.Abs(pVector.X) + Math.Abs(pVector.Y) > 4.5)
            return false;

        return true;
    }

    /// <summary>
    /// Create the homesystem for each player with startposition given by player-index in Array.
    /// The distribution starts with north (0,-1) and works clockwise.
    /// </summary>
    private void buildStarmapForPlayers(Player[] pPlayers)
    {
        int players = pPlayers.Length;
        Vector[] playerPos = getStartPositions(players);
        for (int pos = 0; pos < players; pos++)
        {
            Systems homesystem = pPlayers[pos].getHomesystem();
            homesystem.setVector(playerPos[pos]);
            systemsList[pos + 1] = homesystem;
        }
    }

    /// <summary>
	/// Return the starpositions for the number of players.
	/// </summary>
	private Vector[] getStartPositions(int pPlayers)
    {
        Vector[] playerPositions = new Vector[pPlayers];
        switch (pPlayers)
        {
            case 6:
                playerPositions = new Vector[]
                    {
                        new Vector(0, -3),
                        new Vector (3, -1.5),
                        new Vector (3, 1.5),
                        new Vector (0, 3),
                        new Vector (-3, 1.5),
                        new Vector (-3, -1.5)
                    };
                break;
            case 5:
                Console.WriteLine("Karte für 5 Spieler noch nicht implementiert.");
                break;
            case 4:
                Console.WriteLine("Karte für 4 Spieler noch nicht implementiert.");
                break;
            case 3:
                Console.WriteLine("Karte für 3 Spieler wird nicht implementiert.");
                break;
            default:
                Console.WriteLine("FEHLER: Keine bekannte Spielerzahl angegben!!!");
                break;
        }
        return playerPositions;
    }

    /// <summary>
    /// Search in Starmap for a System with the given Vector and give it back.
    /// </summary>
    private Systems getSystemByVector(Vector pVector)
    {
        if (pVector != null)
        {
            foreach (Systems system in systemsList)
            {
                if (system != null)
                {
                    if (system.getVector().Equals(pVector))
                    {
                        return system;
                    }
                }
            }
        }
        return null;
    }

    private void initSystemsOld()
    {
        systemsList[0] = new Systems("1", new Vector(0, 0));
        int counter = 1;
        for (int sysNr = 0; sysNr < systemsList.Length; sysNr++)
        {
            for (int dirNr = 0; dirNr < Systems.directions.Length; dirNr++)
            {
                if (systemsList[sysNr] != null)
                {
                    if (systemsList[sysNr].getNeighbours()[dirNr] == null)
                    {
                        // Vector des aktuellen Systems plus Richtungsvector
                        Vector neighVec = Vector.Add(systemsList[sysNr].getVector(), Systems.directions[dirNr]);

                        // Vector mit |X| oder |Y| groesser 3 sind nicht gestattet, da es ausserhalb der Karte liegen wuerde
                        if (Math.Abs(neighVec.X) > 3 || Math.Abs(neighVec.Y) > 3)
                        {
                            Console.WriteLine("|" + neighVec.X + "| v |" + neighVec.Y + "| > 3");
                        }
                        // Vector mit |X| plus |Y| groesser 4 sind nicht gestattet, da es ausserhalb der Karte liegen wuerde
                        else if (Math.Abs(neighVec.X) + Math.Abs(neighVec.Y) > 4.5)
                        {
                            Console.WriteLine("|" + neighVec.X + "| + |" + neighVec.Y + "| > 4");
                        }
                        // 
                        else
                        {
                            Systems newSystem = new Systems(counter.ToString(), neighVec);
                            systemsList[counter] = newSystem;
                            systemsList[sysNr].getNeighbours()[dirNr] = newSystem;
                            systemsList[counter].getNeighbours()[(dirNr + 3) % 6] = systemsList[sysNr];

                            int dirNrLeft = ((dirNr - 1) + 6) % 6;
                            Systems neighLeft = systemsList[sysNr].getNeighbours()[dirNrLeft];
                            if (neighLeft != null)
                            {
                                neighLeft.getNeighbours()[(dirNrLeft + 2) % 6] = systemsList[counter];
                                systemsList[counter].getNeighbours()[((dirNrLeft + 2) + 3) % 6] = neighLeft;
                            }

                            int dirNrRight = (dirNr + 1) % 6;
                            Systems neighRight = systemsList[sysNr].getNeighbours()[dirNrRight];
                            if (neighRight != null)
                            {
                                neighRight.getNeighbours()[(dirNrRight + 4) % 6] = systemsList[counter];
                                systemsList[counter].getNeighbours()[((dirNrRight + 4) + 3) % 6] = neighRight;
                            }

                            counter++;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("System Nr.: " + sysNr + " ist unerwarteter Weise null");
                }
            }
        }
    }

    private void paintStarmap()
    {
        foreach (Systems systems in systemsList)
        {
            if (systems != null)
            {
                starmapShape.Children.Add(systems.getCanvas());
            }
        }
    }

    private Boolean SystemsListContainsSystem(Vector pVector)
    {
        foreach (Systems system in systemsList)
        {
            if (system != null)
            {
                if (system.getVector() == pVector)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public Canvas getStarmapShape()
    {
        return this.starmapShape;
    }

}
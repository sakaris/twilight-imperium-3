﻿using System;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;
using System.Collections;
using System.Windows.Media.Imaging;

class Systems
{
    //public static Vector[] directions = { new Vector(0, -1), new Vector(1, -0.5), new Vector(1, 0.5), new Vector(0, 1), new Vector(-1, 0.5), new Vector(-1, -0.5) };
    public static Vector[] directions = { new Vector(0, -1), new Vector(1, -0.5), new Vector(1, 0.5), new Vector(0, 1), new Vector(-1, 0.5), new Vector(-1, -0.5) };

    private String name;
    private Vector vector;
    private int number;
    private Systems[] neighbours;
    private Canvas canvas;

    public Systems(String name, Vector vector)
    {
        this.name = name;
        this.vector = vector;
        this.neighbours = new Systems[6];

        this.canvas = this.wrapPolygonInCanvas();
    }

    private Canvas wrapPolygonInCanvas()
    {
        Canvas canvas = new Canvas();
        canvas.Children.Add(this.paintSystems());

        Vector newVector1 = Vector.Add(new Vector(4, 4), vector);
        TextBlock textBlock1 = new TextBlock();
        textBlock1.Text = this.name;
        textBlock1.FontSize = 20;
        canvas.Children.Add(textBlock1);
        Canvas.SetTop(textBlock1, newVector1.Y * 120 - 25);
        Canvas.SetLeft(textBlock1, newVector1.X * 100 - 10 + 300); // offset: 300

        Vector newVector2 = Vector.Add(new Vector(4, 4), vector);
        TextBlock textBlock2 = new TextBlock();
        textBlock2.Text = newVector2.X + "x" + newVector2.Y;
        textBlock2.FontSize = 20;
        canvas.Children.Add(textBlock2);
        Canvas.SetTop(textBlock2, newVector2.Y * 120 + 5);
        Canvas.SetLeft(textBlock2, newVector2.X * 100 - 15 + 300); // offset: 300

        return canvas;
    }

    private Polygon paintSystems()
    {
        Polygon polygon = null;

        polygon = new Polygon();
        PointCollection pointCollection = new PointCollection();

        double scaleFactor = 8;
        pointCollection.Add(new Point(scaleFactor * 4, scaleFactor * 7));
        pointCollection.Add(new Point(scaleFactor * 8, scaleFactor * 0));
        pointCollection.Add(new Point(scaleFactor * 4, scaleFactor * -7));
        pointCollection.Add(new Point(scaleFactor * -4, scaleFactor * -7));
        pointCollection.Add(new Point(scaleFactor * -8, scaleFactor * 0));
        pointCollection.Add(new Point(scaleFactor * -4, scaleFactor * 7));

        //polygon.Fill = Brushes.DarkBlue;
        polygon.Fill = new ImageBrush(new BitmapImage(new Uri($"pack://application:,,,/img/pic1399245.jpg")));

        polygon.Points = pointCollection;

        Vector newVector = Vector.Add(new Vector(4, 4), vector);
        Canvas.SetTop(polygon, newVector.Y * 120);
        Canvas.SetLeft(polygon, newVector.X * 100 + 300); // offset: 300

        return polygon;
    }

    public Canvas getCanvas()
    {
        return this.canvas;
    }

    public String getName()
    {
        return name;
    }

    public Vector getVector()
    {
        return vector;
    }

    public void setVector(Vector vector)
    {
        this.vector = vector;
    }

    public Systems[] getNeighbours()
    {
        return neighbours;
    }

    public int getNr()
    {
        return number;
    }
}
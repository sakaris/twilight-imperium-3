﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

public class TileViewModel : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

    public Tile2 t1 = new Tile2();

    private PointCollection mPoints = new PointCollection();
    public PointCollection points
    {

        get {  return mPoints; }
        set
        {
            if (this.points != value)
            {
                mPoints = value;
                points = mPoints; 
            }

            PropertyChanged(this, new PropertyChangedEventArgs("mPoints, TileViewModel class"));
        }
    }

    private Polygon mPolygonTile;
    public Polygon polygonTile
    {
        get
        {
            return mPolygonTile;
        }
        set
        {
            if (mPolygonTile == null)
                mPolygonTile = new Polygon();
            mPolygonTile = value;

            if (polygonTile != mPolygonTile)
                polygonTile = mPolygonTile;

            //Set points to this tileviewmodel instance points
            points = t1.polygon.Points;
        }
    }


    public TileViewModel()
    {
        polygonTile = t1.polygon;
    }
}


public class Tile2
{
    // Statische Klassenvariablen
    public static int tileCounter = 0;
    public static Vector[] directions = { new Vector(-1, 0.5), new Vector(0, 1), new Vector(1, 0.5), new Vector(1, -0.5), new Vector(0, -1), new Vector(-1, -0.5) };

    private Canvas mainCanvas;
    public Polygon polygon;

    //Instanzvariablen
    public Vector vector;
    public int nr;
    public ArrayList neighbouringTiles = new ArrayList();
    public Vector[] neighbouringVectors = new Vector[6];

    public Tile2(int i = 10)
    {
        polygon = new Polygon();

        //PointCollection pointCollection = new PointCollection();

        //double scaleFactor = i;
        //pointCollection.Add(new Point(scaleFactor * 4, scaleFactor * 7));
        //pointCollection.Add(new Point(scaleFactor * 8, scaleFactor * 0));
        //pointCollection.Add(new Point(scaleFactor * 4, scaleFactor * -7));
        //pointCollection.Add(new Point(scaleFactor * -4, scaleFactor * -7));
        //pointCollection.Add(new Point(scaleFactor * -8, scaleFactor * 0));
        //pointCollection.Add(new Point(scaleFactor * -4, scaleFactor * 7));

        ////polygon.Fill = Brushes.DarkBlue;
        //polygon.Fill = new ImageBrush(new BitmapImage(new Uri($"pack://application:,,,/img/pic1399245.jpg")));

        //polygon.Points = pointCollection;

        //Vector newVector = Vector.Add(new Vector(4, 4), vector);
        //Canvas.SetTop(polygon, newVector.Y * 120);
        //Canvas.SetLeft(polygon, newVector.X * 100 + 300); // offset: 300
    }

    public void drawSelfOnto(Canvas mainCanvas)
    {
        this.mainCanvas = mainCanvas;

        PointCollection pointCollection = new PointCollection();

        double scaleFactor = 8;
        pointCollection.Add(new Point(scaleFactor * 4, scaleFactor * 7));
        pointCollection.Add(new Point(scaleFactor * 8, scaleFactor * 0));
        pointCollection.Add(new Point(scaleFactor * 4, scaleFactor * -7));
        pointCollection.Add(new Point(scaleFactor * -4, scaleFactor * -7));
        pointCollection.Add(new Point(scaleFactor * -8, scaleFactor * 0));
        pointCollection.Add(new Point(scaleFactor * -4, scaleFactor * 7));

        //polygon.Fill = Brushes.DarkBlue;
        polygon.Fill = new ImageBrush(new BitmapImage(new Uri($"pack://application:,,,/img/pic1399245.jpg")));

        polygon.Points = pointCollection;

        Vector newVector = Vector.Add(new Vector(4, 4), vector);
        Canvas.SetTop(polygon, newVector.Y * 120);
        Canvas.SetLeft(polygon, newVector.X * 100 + 300); // offset: 300

        adjustCanvasHeightWidthIfPolygonIsOutOfBounce(mainCanvas, newVector);

        mainCanvas.Children.Add(polygon);
    }
    #region Helper draw
    private static void adjustCanvasHeightWidthIfPolygonIsOutOfBounce(Canvas mainCanvas, Vector newVector)
    {
        if (newVector.X * 100 + 300 > mainCanvas.ActualWidth)
            mainCanvas.Width = newVector.X * 100 + 450;
        if (newVector.Y * 120 > mainCanvas.ActualHeight)
            mainCanvas.Height = newVector.Y * 120 + 150;
    }
    #endregion

    public Tile2(Vector vec)
    {

        polygon = new Polygon();

        vector = vec;
        nr = ++tileCounter;

        // neighBouringVectors generieren
        for (int a = 0; a < 6; a++)
        {
            neighbouringVectors[a] = vector + directions[a];
        }
    }

    public Tile2(Vector vec, Canvas mainCanvas)
    {
        polygon = new Polygon();

        vector = vec;
        nr = ++tileCounter;

        drawSelfOnto(mainCanvas);
        // neighBouringVectors generieren
        for (int a = 0; a < 6; a++)
        {
            neighbouringVectors[a] = vector + directions[a];
        }
    }

    //~Tile()
    //{ Tile.tileCounter--; }
}